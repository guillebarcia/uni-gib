<?php

use App\Models\Course;
use Caribe\Helpers\Admin;
use Caribe\Helpers\MultiLanguage;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

////////////////////////
// Courses
////////////////////////
Route::middleware('web')->post('courses/{course}/access', 'FrontController@access');
Route::middleware('web')->get('courses/{course}/access', 'FrontController@accessNotAuthorized'); // Control access from browser
Route::middleware('web')->get('courses/{course}/finish-course', 'FrontController@finishCourse')->name('course.finish');
Route::middleware('web')->get('courses/{course}/{item?}', 'FrontController@render')->name('course.render');


Route::middleware('web')->prefix(Admin::prefix())->group(function () {

    ////////////////////////
    // Admin area
    ////////////////////////

    Route::get('courses', 'CourseController@index')->name('admin.courses.index');
    Route::get('courses/create', 'CourseController@create')->name('admin.courses.create');
    Route::post('courses', 'CourseController@store')->name('admin.courses.store');
    Route::get('courses/{course}/edit', 'CourseController@edit')->name('admin.courses.edit');
    Route::put('courses/{course}', 'CourseController@update')->name('admin.courses.update');
    Route::delete('courses/{course}', 'CourseController@destroy')->name('admin.courses.destroy');

    Route::get('reports/{reportType}/{course?}', 'ReportController@index')->name('admin.reports.index');
    Route::get('reports/{reportType}/{course}/export', 'ReportController@downloadCsv')->name('admin.reports.download-export');
    Route::get('reports/course/{course}/student/{student}', 'ReportController@listStudentAccess')->name('admin.reports.student-access');
    Route::put('reports/{course}/reopen-course', 'ReportController@editStudentAccessForCourse')->name('admin.reports.edit-course-status');
});
