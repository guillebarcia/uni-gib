@extends('caribe::layouts.auth')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-7 col-lg-5 col-xl-5">
        <section id="card_2">
            <div class="card">
                <div class="card-body">
                    <h1>Canvas API settings</h1>

                    <form method="POST">
                        <fieldset>
                            <label>
                                Canvas API Key
                                <input type="text" name="canvas_api_key" value="" required />
                            </label>
                            <label>
                                Canvas API Domain
                                <input type="text" name="canvas_api_domain" value="" placeholder="canvas.domain.com" required />
                            </label>
                        </fieldset>
                        <br />
                        <p><input type="submit" value="Proceed" data-loading-text="Installing…" /></p>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
