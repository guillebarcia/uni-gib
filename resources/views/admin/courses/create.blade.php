@extends('caribe::layouts.admin')

@section('content')

<div id="content" class="container-fluid">
    
    <div class="jumbotron">
        
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.courses.index') }}">All Courses</a>
            </li>
            <li class="breadcrumb-item active">Add</li>
        </ol>
        
        <div class="row">
            <div class="col">
                <h3>Add new Course</h3>
            </div>
        </div>
        
    </div>
    
    <form method="POST" action="{{ route('admin.courses.store') }}" class="row" data-callback="formCallback">
        
        @include('admin.courses._form', ['editing' => false])
        
        <div id="sidebar_content" class="col-lg col-md-12">
            <div class="card">
                <div class="card-header">
                    Actions
                </div>
                <div class="card-body">
                    <button type="submit" class="btn btn-success btn-block" data-loading-text="Creating…">Create Course</button>
                </div>
            </div>
        </div>
        
    </form>
    
</div>

@endsection

@push('scripts')
<!-- JS -->
<script type="text/javascript">
function formCallback(data) {
    CommonFunctions.notificationSuccessStayOrBack(data.message, data.entryUrl, "{{route('admin.courses.index')}}");
}
</script>
@endpush
