<div class="col-lg col-md-12">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6"
                            @if (!multilanguage()) style="display:none" @endif>
                            <label for="f-locale">
                                <i class="fa fa-flag"></i> Locale
                            </label>
                            <select name="locale" class="form-control" id="f-locale">
                                <option value="">- Please select -</option>
                                @foreach (languages() as $language)
                                    <option value="{{ $language->locale }}"
                                        @if (($course->locale ?? '') == $language->locale || !multilanguage()) selected @endif>
                                        {{ $language->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <label for="f-title">Title</label>
                            <input type="text" class="form-control" id="f-title" name="title"
                                value="{{ $course->title ?? '' }}">
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <label for="f-tag">Tag (unique identifier)</label>
                            <input type="text" class="form-control" id="f-tag" name="tag"
                                value="{{ $course->tag ?? '' }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <label for="f-css-id">CSS id</label>
                            <input type="text" class="form-control" id="f-css-id" name="css_id"
                                value="{{ $course->css_id ?? '' }}">
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <label for="f-css-id">CSS class</label>
                            <input type="text" class="form-control" id="f-css-class" name="css_class"
                                value="{{ $course->css_class ?? '' }}">
                        </div>
                    </div>
                    <div class="row align-items-end">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <label for="f-hide-unavailable"><input type="checkbox" class="form-control"
                                    id="f-hide-unavailable" name="published"
                                    {{ $course->published ?? true ? 'checked' : '' }}> Publish course</label>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">

                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <button type="button" class="addPage btn btn-info btn-rad">
                                        <i class="fa fa-file-text-o"></i> Add Page
                                    </button>
                                </div>
                            </div>

                            <div id="menuItems" class="dd itemsCourse">
                                <ol class="dd-list"></ol>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-none">

    <div id="editPageModal">
        <div class="modal colored-header caribe-modal caribe-menu-modal">
            <div class="modal-header">
                <i class="fa fa-file-text-o"></i> Edit Page
            </div>
            <div class="modal-body form form-horizontal">

                <!-- Page -->
                <div class="form-group">
                    <label class="control-label"><span class="pageInfo form-text"></span></label>
                </div>

                <!-- Title -->
                <div class="form-group">
                    <label class="control-label">Title</label>
                    <input type="text" class="form-control" data-name="title" />
                </div>

                <div class="form-group">
                    <div class="row">
                        <!-- CSS id -->
                        <div class="col-12 col-sm-6 col-md-6">
                            <label class="control-label">CSS Id</label>
                            <input type="text" class="form-control" data-name="css_id" />
                        </div>

                        <!-- CSS class -->
                        <div class="col-12 col-sm-6 col-md-6">
                            <label class="control-label">CSS Class</label>
                            <input type="text" class="form-control" data-name="css_class" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger cancelButton">Cancel</button>
                <button type="button" class="btn btn-success saveButton">Save</button>
            </div>
        </div>
    </div>

    <ul id="itemHtml">
        <li class="dd-item dd3-item">
            <div class="dd-handle"><i class="main-icon"></i></div>
            <div class="dd3-content">
                <span></span>
                <small></small>
                <button type="button" class="btn editMenuItem"><i class="fa fa-pencil size-l"></i></button>
                <button type="button" class="btn hideMenuItem"><i class="fa size-l" data-on-class="fa-eye"
                        data-off-class="fa-eye-slash"></i></button>
                <button type="button" class="btn deleteMenuItem"><i class="fa fa-trash-o size-l"></i></button>
            </div>
        </li>
    </ul>

</div>

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/caribe-menus/vendor/jquery.nestable2/jquery.nestable.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.courses._form.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript">
        var items = @json(isset($course) ? $course->items : []);
        var pageSelectionUrl = @json(route('admin.pages.select'));
    </script>
    <script type="text/javascript"
        src="{{ asset('vendor/caribe-menus/vendor/jquery.nestable2/jquery.nestable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin.courses._form.js') }}"></script>
@endpush
