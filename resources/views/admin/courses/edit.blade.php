@extends('caribe::layouts.admin')

@section('content')

<div id="content" class="container-fluid">
    
    <div class="jumbotron">
        
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.courses.index') }}">All Courses</a>
            </li>
            <li class="breadcrumb-item active">Edit</li>
        </ol>
        
        <div class="row">
            <div class="col">
                <h3>Edit Course</h3>
            </div>
        </div>
        
    </div>
    
    <form method="POST" action="{{ route('admin.courses.update', $course->id) }}" class="row" data-callback="formCallback">
        <input type="hidden" name="_method" value="PUT" />
        
        @include('admin.courses._form', ['editing' => true])
        
        <div id="sidebar_content" class="col-lg col-md-12">
            <div class="card">
                <div class="card-header">
                    Actions
                </div>
                <div class="card-body">
                    <button type="submit" class="btn btn-success btn-block" data-loading-text="Saving…">Save Course</button>
                    <hr>
                    <button type="button" id="deleteEntry" class="btn btn-outline-danger btn-block">
                        Delete Course
                    </button>
                </div>
            </div>
        </div>
        
    </form>
</div>

@endsection

@push('scripts')
<!-- JS -->
<script type="text/javascript">
function formCallback(data) {
    CommonFunctions.notificationSuccessStayOrBack(data.message, null, "{{route('admin.courses.index')}}");
}

$('#deleteEntry').click(function() {
    CommonFunctions.notificationConfirmDelete(
        "You are about to delete this course. This action cannot be undone",
        'Delete course',
        "{{ route('admin.courses.destroy', $course->id) }}",
        function(data) {
            CommonFunctions.notificationSuccessRedirect(data, "{{route('admin.courses.index')}}");
        }
    );
});
</script>
@endpush
