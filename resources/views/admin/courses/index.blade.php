@extends('caribe::layouts.admin')

@section('content')

    <div id="content" class="container-fluid">

        <div class="jumbotron">

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">All Courses</li>
            </ol>

            <div class="row">
                <div class="col">
                    <h3>
                        Courses
                        <a href="{{ route('admin.courses.create') }}" class="btn btn-outline-light">
                            <i class="icon-plus icons icon_l"></i> Add Course
                        </a>
                    </h3>
                </div>
            </div>

        </div>

        @if (multilanguage())
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="f-locale">
                            <i class="fa fa-flag"></i> Locale
                        </label>
                        <select name="locale" class="form-control" id="f-locale">
                            @foreach (languages() as $language)
                                <option value="{{ $language->locale }}" @if ($locale == $language->locale) selected @endif>{{ $language->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <table class="table data_table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Tag</th>
                        <th>Nº Pages</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses as $course)
                        <tr data-href="{{ route('admin.courses.edit', $course->id) }}">
                            <td>{{ $course->title }}</td>
                            <td>{{ $course->tag }}</td>
                            <td>{{ count($course->items) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#f-locale').change(function() {
                window.location = @json(route('admin.courses.index')) + '?locale=' + $(this).val();
            });
        });
    </script>
@endpush
