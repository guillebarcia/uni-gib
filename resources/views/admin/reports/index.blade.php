@extends('caribe::layouts.admin')

@section('content')

    <div id="content" class="container-fluid">
        <div class="jumbotron">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>

                @if ($reportType === 'access')
                    <li class="breadcrumb-item active">All Access</li>
                @endif

                @if ($reportType === 'pages')
                    <li class="breadcrumb-item active">All Visits</li>
                @endif
            </ol>

            <div class="row">
                <div class="col">
                    <h3>
                        @if ($reportType === 'access')
                            Students Access
                        @endif

                        @if ($reportType === 'pages')
                            Pages Popularity
                        @endif

                        <a href="{{ route('admin.reports.download-export', ['reportType' => $reportType, 'course' => $courseSelected->id]) }}"
                            class="btn btn-outline-light pull-right">
                            Export
                        </a>
                    </h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="f-course">
                        <i class="fa fa-graduation-cap"></i> Course
                    </label>
                    <select name="course" class="form-control" id="f-course">
                        @foreach ($courseList as $course)
                            <option value="{{ $course->id }}" @if ($courseSelected->id == $course->id) selected @endif>
                                {{ $course->title }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        @if (multilanguage())
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="f-locale">
                            <i class="fa fa-flag"></i> Locale
                        </label>
                        <select name="locale" class="form-control" id="f-locale">
                            @foreach (languages() as $language)
                                <option value="{{ $language->locale }}" @if ($locale == $language->locale) selected @endif>
                                    {{ $language->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            @if ($reportType === 'access')
                @foreach ($reports as $report)
                    <form method="POST" id="update-report-{{ $report['id'] }}"
                        action="{{ route('admin.reports.edit-course-status', $courseSelected->id) }}"
                        data-callback="formCallback">
                        <input type="hidden" name="_method" value="PUT" />
                        <input type="hidden" name="report" value="{{ $report['id'] }}" />
                    </form>
                @endforeach
            @endif

            <table class="table data_table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        @if ($reportType === 'access')
                            <th>Name</th>
                            <th>Email</th>
                            <th>First Access</th>
                            <th>Last Access</th>
                            <th>Time Course Finished</th>
                            <th>Current Page</th>
                            <th>Nº Visits in last 30 days</th>
                        @endif

                        @if ($reportType === 'pages')
                            <th>Title</th>
                            <th>Page Nº in the Course</th>
                            <th>Nº Visits</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reports as $report)
                        @if ($reportType === 'access')
                            <tr>
                                <td>
                                    {{ $report['name'] }}
                                    <a class="btn btn-light btn-sm"
                                     href="{{ route('admin.reports.student-access', ['course' => $courseSelected->id, 'student' => $report['student_id']]) }}">
                                        <i class="fa fa-eye" title="Student access details"></i>
                                    </a>
                                </td>
                                <td>{{ $report['email'] }}</td>
                                <td>{{ $report['firstAccess'] }}</td>
                                <td>{{ $report['lastAccess'] }}</td>
                                <td>
                                    @if (!is_null($report['timeCourseFinished']))
                                        {{ $report['timeCourseFinished'] }}
                                        <input type="hidden" name="option" form="update-report-{{ $report['id'] }}"
                                            value="open" />
                                        <button class="btn btn-light btn-sm pull-right"
                                            form="update-report-{{ $report['id'] }}">
                                            <i class="fa fa-lock" title="Open the course for the student"></i>
                                        </button>
                                    @else
                                        <input type="hidden" name="option" form="update-report-{{ $report['id'] }}"
                                            value="close" />
                                        <button class="btn btn-light btn-sm pull-right"
                                            form="update-report-{{ $report['id'] }}">
                                            <i class="fa fa-unlock-alt" title="Close the course for the student"></i>
                                        </button>
                                    @endif
                                </td>
                                <td>{{ $report['currentPage'] }}</td>
                                <td>{{ $report['numVisits'] }}</td>
                        @endif

                        @if ($reportType === 'pages')
                            <tr>
                                <td>{{ $report['title'] }}</td>
                                <td>{{ $report['position'] }}</td>
                                <td>{{ $report['numVisits'] }}</td>
                        @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#f-locale').change(function() {
                window.location = @json(route('admin.reports.index', ['reportType' => $reportType, 'course' => $courseSelected->id])) + '?locale=' + $(this).val();
            });
            $('#f-course').change(function() {
                window.location = @json(route('admin.reports.index', ['reportType' => $reportType])) + '/' + $(this).val();
            });
        });

        function formCallback(data) {
            CommonFunctions.notificationSuccessRedirect(data.message, @json(route('admin.reports.index', ['reportType' => $reportType, 'course' => $courseSelected->id])));
        }
    </script>
@endpush
