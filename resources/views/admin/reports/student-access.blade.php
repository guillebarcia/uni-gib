@extends('caribe::layouts.admin')

@section('content')
    <div id="content" class="container-fluid">

        <div class="jumbotron">

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.reports.index', ['reportType' => 'access', 'course' => $course->id]) }}">
                        Access Reports
                    </a>
                </li>
                <li class="breadcrumb-item active">Student Access</li>
            </ol>
        </div>

        <div class="row">
            <table class="table data_table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Page</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Duration (In min)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($studentReports as $index => $studentReport)
                        <tr>
                            <td>{{ $student->name }}</td>
                            <td>{{ $course->title }}</td>
                            <td>{{ $studentReport->page_visited }}</td>
                            <td>{{ $studentReport->access_time }}</td>
                            <td>
                                {{ is_null($studentReport->end_date_time) ? 'Session finished' : $studentReport->end_date_time }}
                            </td>
                            <td>
                                {{ is_null($studentReport->duration) ? 'N/A' : $studentReport->duration }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
@endsection
