<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $metaTitle ?? $title ?? config('app.name', 'Site') }}</title>
    <meta name="description" content="{{ $metaDescription ?? '' }}">
    <meta name="robots" content="NOODP">

    <!-- Facebook OG Tags -->
    <meta name="og:title" content="{{ $metaTitle ?? $title ?? config('app.name', 'Site') }}">
    <meta name="og:description" content="{{ $metaDescription ?? '' }}">
    <meta property="og:site_name" content="{{ config('app.name', 'Site') }}" />
    <meta name="og:image" content="{{ $metaImage ?? '' }}">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="{{ $metaTitle ?? $title ?? config('app.name', 'Site') }}">
    <meta name="twitter:description" content="{{ $metaDescription ?? '' }}">
    <meta name="twitter:image:src" content="{{ $metaImage ?? '' }}">

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('assets/favicon/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/favicon/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/favicon/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/favicon/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('assets/favicon/apple-touch-icon-60x60.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('assets/favicon/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('assets/favicon/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('assets/favicon/apple-touch-icon-152x152.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon-196x196.png') }}" sizes="196x196" />
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon-96x96.png') }}" sizes="96x96" />
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon-128.png') }}" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/mstile-144x144.png') }}" />
    <meta name="msapplication-square70x70logo" content="{{ asset('assets/favicon/mstile-70x70.png') }}" />
    <meta name="msapplication-square150x150logo" content="{{ asset('assets/favicon/mstile-150x150.png') }}" />
    <meta name="msapplication-wide310x150logo" content="{{ asset('assets/favicon/mstile-310x150.png') }}" />
    <meta name="msapplication-square310x310logo" content="{{ asset('assets/favicon/mstile-310x310.png') }}" />

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/bootstrap-4.3.1/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/caribe/vendor/font-awesome-4.7.0/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/flexslider-2.7.1.0/flexslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/slick-1.8.1/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/slick-1.8.1/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/caribe-visual-editor/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/aos/css/aos.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/accesibility.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/caribe/css/front.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/styles.css') }}">
</head>
<body>
    <div id="accesibilityPanel">
        <div class="row">
            <div class="col-12">
                <h2 class="uppercase"><strong>{{ __('Accessibility') }}</strong></h2>
                <p class="text-right"><button type="button" id="closeAccesibilityPanel">X</button></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3>{{ __('Font size') }}</h3>
                <button type="button" class="accesibilityFontDecrease">-</button>
                <span class="accesibilityFontSize"></span>
                <button type="button" class="accesibilityFontIncrease">+</button>
                <button type="button" class="accesibilityFontReset">{{ __('Reset') }}</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>{{ __('Filters') }}</h3>
                <ul>
                    <li>
                        <label><input type="radio" name="accessibilityFiltersOptions" value="" checked /> {{ __('None') }}</label>
                        <label><input type="radio" name="accessibilityFiltersOptions" value="grayscale" /> {{ __('Grayscale') }}</label>
                        <label><input type="radio" name="accessibilityFiltersOptions" value="invert" /> {{ __('Inverted Colors') }}</label>
                        <label><input type="radio" name="accessibilityFiltersOptions" value="w-blue" /> {{ __('Without Blue') }}</label>
                        <label><input type="radio" name="accessibilityFiltersOptions" value="w-green" /> {{ __('Without Green') }}</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>{{ __('Highlight') }}</h3>
                <ul>
                    <li>
                        <label><input type="radio" name="accessibilityHighlightOptions" value="" checked /> {{ __('None') }}</label>
                        <label><input type="radio" name="accessibilityHighlightOptions" value="links" /> {{ __('Links') }}</label>
                        <label><input type="radio" name="accessibilityHighlightOptions" value="titles" /> {{ __('Titles') }}</label>
                        <label><input type="radio" name="accessibilityHighlightOptions" value="text" /> {{ __('Just text') }}</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>{{ __('Colour') }}</h3>
                <ul>
                    <li>
                        <label><input type="radio" name="accessibilityColourOptions" value="" checked /> {{ __('Default') }}</label>
                        <label><input type="radio" name="accessibilityColourOptions" value="black" /> {{ __('Black on White') }}</label>
                        <label><input type="radio" name="accessibilityColourOptions" value="yellow" /> {{ __('Yellow on Black') }}</label>
                        <label><input type="radio" name="accessibilityColourOptions" value="green" /> {{ __('Green on Black') }}</label>
                        <label><input type="radio" name="accessibilityColourOptions" value="white" /> {{ __('White on Black') }}</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>Zoom</h3>
                <button type="button" class="accesibilityZoomDecrease">-</button>
                <span class="accesibilityZoomScale"></span>
                <button type="button" class="accesibilityZoomIncrease">+</button>
                <button type="button" class="accesibilityZoomReset">{{ __('Reset') }}</button>
            </div>
        </div>
    </div>

    <div id="accesibilityArea">
        <header>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-8">
                        <a href="{{ url('/') }}" title="University of Gibraltar" class="logo animate__animated animate__fadeIn"><img src="{{ asset('assets/img/logo-white.png') }}" alt="University of Gibraltar White Logo"/></a>
                    </div>
                    <div class="col-4 text-right">
                        <p class="mb-0 pb-0">
                            <a href="#" title="{{ __('Accessibility') }}" class="animate__animated animate__fadeIn" id="accessibility-btn"><i class="fa fa-eye-slash"></i></a>
                            <a href="#" title="{{ __('Open Menu') }}" class="animate__animated animate__fadeIn" id="open-menu"><i class="fa fa-bars"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </header>

        <section id="content">
            @yield('content')
        </section>

        @yield('footer')

        <div id="offCanvas">
            <button id="close-menu"></button>
            <div class="courses-navigation">
                @yield('course-menu')
            </div>
        </div>
    </div>

    <!-- JS -->
    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}";
    </script>
    <script type="text/javascript" src="{{ asset('vendor/caribe/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/caribe/vendor/bootstrap/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/bootstrap-4.3.1/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendor/flexslider-2.7.1.0/jquery.flexslider-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery.matchHeight.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/slick-1.8.1/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/aos/js/aos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/accessibility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery.rwdImageMaps.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/site.js') }}"></script>
    @stack('course-scripts')
</body>
</html>