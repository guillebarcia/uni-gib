@extends('layouts.default')

@section('content')
    <div class="section-title container-fluid">
        <div class="row">
            <div class="container">
                <div class="align-items-end row">
                    <div class="col-12 col-lg-11">
                        <div>
                            <p class="animate__animated animate__fadeInUp">404</p>
                            <h2 class="animate__animated animate__fadeInUp">PAGE NOT FOUND</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container pb-5 pt-5" id="error">
        <div class="row align-items-center">
            <div class="col-12 text-center">
                <h1 class="xl">:(</h1>
                <h4 class="pt-4">The page you requested does not exist</h4>
                <p>Please press the back button or choose from the menu.</p>
                <p class="pt-3"><a href="{{ url('/') }}" title="" title="Go to Homepage" class="button"><i class="fa fa-home"></i> Home</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url()->previous() }}" title="Go Back" class="button">« Back</a></p>
            </div>
        </div>
    </div>
@endsection