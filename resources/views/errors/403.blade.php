@extends('layouts.default')

@section('content')
<div class="section-title container-fluid">
    <div class="row">
        <div class="container">
            <div class="align-items-end row">
                <div class="col-12 col-lg-11">
                    <div>
                        <p class="animate__animated animate__fadeInUp">403</p>
                        <h2 class="animate__animated animate__fadeInUp">Access denied.</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container pb-5 pt-5" id="error">
    <div class="row align-items-center">
        <div class="col-12 text-center">
            <h1 class="xl">:(</h1>
            <h4 class="pt-4">{{ $exception->getMessage() }}</h4>
            <p class="pt-3">
                <a href="https://{{ env('CANVAS_DOMAIN')}}" title="Go to Canvas" class="button">
                    « Go to Canvas
                </a>
            </p>
        </div>
    </div>
</div>
@endsection