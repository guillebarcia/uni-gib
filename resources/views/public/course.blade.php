@extends('layouts.default')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @if ($page != null)
                {!! shortcode($page->content) !!}
            @endif
        </div>
    </div>
@endsection

@section('course-menu')
    {!! course_menu($courseTag) !!}
@endsection

@section('footer')
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    @if ($itemPosition > 1)
                        <p>
                            <a href="{{ route('course.render', ['courseTag' => $courseTag, 'item' => $itemPosition - 1]) }}"
                                title="Previous" class="page-select disabled animate__animated animate__fadeInLeft">
                                <i class="fa fa-angle-left"></i>
                            </a>
                        </p>
                    @endif
                </div>
                <div class="col-6 text-right">
                    @if ($itemPosition != $totalPages)
                        <p>
                            <a href="{{ route('course.render', ['courseTag' => $courseTag, 'item' => $itemPosition + 1]) }}"
                                title="Next" class="page-select animate__animated animate__fadeInRight">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </p>
                    @endif
                </div>
                <div class="col-12">
                    @php
                        $progress = ($itemPosition * 100) / $totalPages;
                    @endphp
                    <div class="progress-bar animate__animated animate__fadeInUp">
                        <div class="progress" style="width: {{ $progress }}%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection

@push('course-scripts')
    <script type="text/javascript">
        $('#finish-course').click(function() {
            var form = $("<form/>", {
                action: @json(route('course.finish', $courseTag))
            });
            $.ajax({
                url: form.attr('action'),
                async: false,
            });
            window.close();
        });
    </script>
@endpush
