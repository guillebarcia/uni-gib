<div id="{{ $accordion->id }}" @if ($accordion->class) class="{{ $accordion->class }}" @endif role="tablist">
    
    @foreach ($accordion->items as $item)
    <div class="card">
        <div class="card-header" role="tab" id="{{ $item->headerId }}">
            <h5 class="mb-0">
                <a class="collapsed" data-toggle="collapse" href="#{{ $item->collapseId }}" aria-expanded="false" aria-controls="{{ $item->collapseId }}">
                    @if ($item->excerpt)
                    <span class="number">{{ $item->excerpt }}</span>
                    @endif
                    {{ $item->title }} 
                </a>
            </h5>
        </div>
        <div id="{{ $item->collapseId }}" class="collapse" role="tabpanel" aria-labelledby="{{ $item->headerId }}" data-parent="#{{ $accordion->id }}">
            <div class="card-body">
                

                {!! $item->content !!}
            </div>
        </div>
    </div>
    @endforeach
    
</div>