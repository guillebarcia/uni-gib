<?php

return [
    
    ///////////////////
    // Courses
    ///////////////////
    
    'course_add_success' => 'Course has been created successfully',
    'course_edit_success' => 'Course has been updated successfully',
    'course_delete_success' => 'Course has been deleted successfully',

    ///////////////////
    // Reports
    ///////////////////
    
    'student_access_edit_success' => 'The student access has been updated successfully',
];
