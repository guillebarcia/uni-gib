<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('type_id');
            
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->unsignedInteger('page_id')->nullable();
            $table->string('link')->nullable();
            $table->boolean('target_blank')->nullable();
            
            $table->string('css_id')->nullable();
            $table->string('css_class')->nullable();
            
            $table->unsignedInteger('parent_id')->nullable();
            $table->integer('position');
            $table->boolean('hidden');
            
            $table->foreign('course_id')->references('id')->on('course')->onDelete('cascade');
            $table->foreign('page_id')->references('id')->on('page')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('course_item')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_item');
    }
}
