<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->string('locale', 10);
            $table->string('title');

            $table->string('css_id')->nullable();
            $table->string('css_class')->nullable();
            $table->boolean('published');

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['tag', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course');
    }
}
