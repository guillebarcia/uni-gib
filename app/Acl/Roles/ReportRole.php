<?php

namespace App\Acl\Roles;

use Caribe\Contracts\Acl\Role;

class ReportRole implements Role
{
    /**
     * @inheritdoc
     */
    public function getActions()
    {
        return [
            // Access Reports
            'App\Http\Controllers\ReportController@index',
            'App\Http\Controllers\ReportController@downloadCsv',
            'App\Http\Controllers\ReportController@editStudentAccessForCourse',
            'App\Http\Controllers\ReportController@listStudentAccess'
        ];
    }
}
