<?php

namespace App\Acl\Roles;

use Caribe\Contracts\Acl\Role;

class CourseRole implements Role
{
    /**
     * @inheritdoc
     */
    public function getActions()
    {
        return [
            'App\Http\Controllers\CourseController@index',
            'App\Http\Controllers\CourseController@create',
            'App\Http\Controllers\CourseController@store',
            'App\Http\Controllers\CourseController@edit',
            'App\Http\Controllers\CourseController@update',
            'App\Http\Controllers\CourseController@destroy',
        ];
    }
}
