<?php

namespace App\Models;

use Caribe\Models\Model;

class StudentReport extends Model
{
    /**
     * If the table has or not timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Get the connected student
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
