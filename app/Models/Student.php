<?php

namespace App\Models;

use Caribe\Models\Model;

class Student extends Model
{
    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Get the courses of the student
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
        return $this->hasMany(CourseStudent::class);
    }

    /**
     * Get the student reports
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studentReports()
    {
        return $this->hasMany(StudentReport::class);
    }
}
