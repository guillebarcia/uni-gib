<?php

namespace App\Models;

use Caribe\Models\Model;

class CourseStudent extends Model
{
    /**
     * If the table has or not timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the connected Student
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Get the connected Course
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
