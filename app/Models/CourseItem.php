<?php

namespace App\Models;

use Caribe\Models\Model;

class CourseItem extends Model
{
    /**
     * If the table has or not timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = ['id', 'course_id', 'position'];

    /**
     * Get the connected page (if any)
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(\Caribe\Pages\Models\Page::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
