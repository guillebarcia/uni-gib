<?php

namespace App\Console;

use Composer\Composer;
use Composer\Factory;
use Composer\IO\IOInterface;
use Composer\Json\JsonFile;
use Composer\Package\Link;
use Composer\Package\Version\VersionParser;

class Installer
{
    const INSTALL_FULL_CMS = 'full';
    const INSTALL_MINIMAL = 'minimal';
    
    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @var array
     */
    private $composerDefinition;
    
    /**
     * @var JsonFile
     */
    private $composerJson;
    
    /**
     * @var Link[]
     */
    private $composerRequires;
    
    /**
     * @var Composer\Package\RootPackageInterface
     */
    private $rootPackage;
    
    /**
     * Constructor
     */
    public function __construct(IOInterface $io, Composer $composer)
    {
        $this->io = $io;
        
        $composerFile = Factory::getComposerFile();
        
        $this->composerJson = new JsonFile($composerFile);
        $this->composerDefinition = $this->composerJson->read();
        $this->rootPackage = $composer->getPackage();
        $this->composerRequires = $this->rootPackage->getRequires();
    }
    
    /**
     * Run the installer
     *
     * @return void
     */
    public function run()
    {
        $installType = $this->requestInstallType();
        $extraPackages = $this->getPackagesFor($installType);
        if ($extraPackages) {
            foreach ($extraPackages as $package => $version) {
                $this->addPackage($package, $version);
            }
            $this->finalize();
        }
    }
    
    /**
     * Prompt for the installation type
     *
     * @return string
     */
    private function requestInstallType()
    {
        $query = [
            "\n <question>What type of installation would you like?</question>\n",
            "  [<comment>1</comment>] Full CMS (most websites)\n",
            "  [<comment>2</comment>] Minimal (systems with no front side)\n",
            ' Please make your selection <comment>(1)</comment>: ',
        ];
        
        while (true) {
            $answer = $this->io->ask(implode($query), '1');
            
            switch ($answer) {
                case '1' :
                    return self::INSTALL_FULL_CMS;
                case '2' :
                    return self::INSTALL_MINIMAL;
                default:
                    $this->io->write('<error>Invalid answer</error>');
            }
        }
    }

    /**
     * Get packages for the installation type
     *
     * @return array
     */
    private function getPackagesFor(string $installType)
    {
        switch ($installType) {
            case self::INSTALL_MINIMAL:
                return [];
            case self::INSTALL_FULL_CMS:
                return [
                    'piranhadesigns/caribe-file-manager' => '~0.1',
                    'piranhadesigns/caribe-shortcodes' => '~0.1',
                    'piranhadesigns/caribe-html-editor' => '~0.1',
                    'piranhadesigns/caribe-visual-editor' => '~0.1',
                    'piranhadesigns/caribe-snippets' => '~0.1',
                    'piranhadesigns/caribe-pages' => '~0.1',
                    'piranhadesigns/caribe-menus' => '~0.1',
                ];
        }
    }

    /**
     * Add a package
     * 
     * @param  string  $packageName
     * @param  string  $packageVersion
     * @param  bool  $dev
     * 
     * @return  void
     */
    private function addPackage(string $packageName, string $packageVersion, bool $dev = false)
    {
        // Get the version constraint
        $versionParser = new VersionParser();
        $constraint = $versionParser->parseConstraints($packageVersion);
        
        $this->composerDefinition['require'][$packageName] = $packageVersion;
        $this->composerRequires[$packageName] = new Link('__root__', $packageName, $constraint, 'requires', $packageVersion);
    }
    
    /**
     * Finalize the process
     *
     * Writes the current JSON state to composer.json
     *
     * @return void
     */
    private function finalize()
    {
        $this->rootPackage->setRequires($this->composerRequires);
        
        // Update composer definition
        $this->composerJson->write($this->composerDefinition);
    }
}
