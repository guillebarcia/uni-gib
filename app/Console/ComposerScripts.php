<?php

namespace App\Console;

use Composer\Script\Event;

class ComposerScripts
{
    /**
     * Installation wizard
     *
     * @param  \Composer\Script\Event  $event
     * 
     * @return void
     */
    public static function installWizard(Event $event)
    {
        $installer = new Installer($event->getIO(), $event->getComposer());
        $installer->run();
    }
    
    /**
     * Handle the post-create-project Composer event
     *
     * @param  \Composer\Script\Event  $event
     * 
     * @return void
     */
    public static function postCreateProject(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        require_once "$vendorDir/autoload.php";

        static::generateHtaccess($vendorDir);
    }
    
    /**
     * Generate an htaccess file from .dist version
     *
     * @param  string  $vendorDir
     *
     * @return void
     */
    protected static function generateHtaccess(string $vendorDir)
    {
        $publicDir = "$vendorDir/../public_html";
        if (!is_file("$publicDir/.htaccess") && is_file("$publicDir/.htaccess.dist") ) {
            $folders = explode('\\', dirname($vendorDir));
            if (count($folders) > 1) {
                $projectFolder = $folders[count($folders) - 1];
                
                // Write file
                $content = file_get_contents("$publicDir/.htaccess.dist");
                $content = str_replace('RewriteBase /', "RewriteBase /$projectFolder/", $content);
                file_put_contents("$publicDir/.htaccess", $content);
            }
        }
    }
}
