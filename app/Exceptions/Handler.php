<?php

namespace App\Exceptions;

use Caribe\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * URL to redirect to if unauthenticated
     *
     * @var string
     */
    protected $unauthenticatedRedirectTo = '/';

}
