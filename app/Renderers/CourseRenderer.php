<?php

namespace App\Renderers;

use Caribe\Menus\Contracts\Renderer as RendererContract;
use Illuminate\Contracts\Foundation\Application;
use App\Models\CourseItem;
use Illuminate\Support\Facades\URL;

class CourseRenderer implements RendererContract
{
    /**
     * Application
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;
    
    /**
     * Create a new instance
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * 
     * @return void
     */
    public function __construct(Application  $app)
    {
        $this->app = $app;
    }
    
    /**
     * @inheritdoc
     */
    public function render(array $courseItems, string $menuCssId = null, string $menuCssClass = null)
    {
        list($html, $childrenActive) = $this->renderItems($courseItems, $menuCssId, $menuCssClass);
        return $html;
    }
    
    /**
     * Render a list of items
     *
     * @param  array  $courseItems
     * @param  string|null  $ulId
     * @param  string|null  $ulClass
     * 
     * @return array
     */
    protected function renderItems(array $courseItems, string $ulId = null, string $ulClass = null)
    {
        $html = '';
        $childrenActive = false;
        $childrenPending = true;
        foreach ($courseItems as $item) {
            list($itemHtml, $isActive, $isDone) = $this->renderLink($item['item']);
            $childrenActive = $childrenActive ?: $isActive;
            $childrenPending = !$isDone;
            
            if ($item['children']) {
                list($childrenHtml, $containsActive, $containsPending) = $this->renderItems($item['children']);
                $itemHtml .= $childrenHtml;
                $childrenActive = $childrenActive ?: $containsActive;
                $childrenPending = $containsPending ?: $childrenPending;
            }
            
            $classes = [];
            if ($isActive) {
                $classes[] = $this->activeClass();
            }
            if ($item['item']['css_class'] ?? null) {
                $classes[] = $item['item']['css_class'];
            }
            if ($item['children']) {
                $classes[] = $this->parentClass();
                if ($containsActive) {
                    $classes[] = $this->activeParentClass();
                }
            }
            if (!$childrenPending) {
                $classes[] = $this->doneClass();
            }
            $html .= $this->renderLi($itemHtml, $classes, $item['item']['css_id'] ?? null);
        }
        
        return [$this->renderUl($html, explode(' ', $ulClass), $ulId), $childrenActive, $childrenPending];
    }
    
    /**
     * Render a link
     *
     * @param  CourseItem  $courseItem
     * 
     * @return array
     */
    protected function renderLink(CourseItem $courseItem)
    {
        $link = course_item_url($courseItem);
        $currentPosition = last(request()->segments());
        
        $html = sprintf(
            '<a href="%s" %s>%s %s</a>', 
            e($link),
            $courseItem->target_blank ? 'target="_blank"' : '',
            e($courseItem->title),
            $courseItem->subtitle ? '<span>' . e($courseItem->subtitle) . '</span>' : ''
        );
        $isActive = URL::current() == $link;
        $isDone = $courseItem->position < $currentPosition;
        
        return [$html, $isActive, $isDone];
    }
    
    /**
     * Render the <li> element
     *
     * @param  string  $content
     * @param  array|null  $classes
     * @param  string|null  $id
     * 
     * @return string
     */
    protected function renderLi(string $content, array $classes = [], string $id = null)
    {
        $liId = $id ? sprintf('id="%s"', $id) : '';
        $liClass = $classes ? sprintf('class="%s"', implode(' ', $classes)) : '';
        return sprintf('<li %s %s>%s</li>', $liId, $liClass, $content);
    }
    
    /**
     * Render the <ul> element
     *
     * @param  string  $content
     * @param  array|null  $classes
     * @param  string|null  $id
     * 
     * @return string
     */
    protected function renderUl(string $content, array $classes = [], string $id = null)
    {
        $ulId = $id ? sprintf('id="%s"', $id) : '';
        $ulClass = $classes ? sprintf('class="%s"', implode(' ', $classes)) : '';
        return sprintf('<ul %s %s>%s</ul>', $ulId, $ulClass, $content);
    }
    
    /**
     * Class when element is active
     * 
     * @return string
     */
    protected function activeClass()
    {
        return 'active';
    }
    
    /**
     * Class when element is done
     * 
     * @return string
     */
    protected function doneClass()
    {
        return 'done';
    }
    
    /**
     * Parent class when a child is active
     * 
     * @return string
     */
    protected function activeParentClass()
    {
        return 'active-parent';
    }
    
    /**
     * Parent class
     * 
     * @return string
     */
    protected function parentClass()
    {
        return 'parent';
    }

}
