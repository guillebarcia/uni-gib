<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Caribe\Helpers\AjaxForm;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;

class SetupCanvas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * 
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('CANVAS_KEY') && env('CANVAS_DOMAIN')) {
            return $next($request);
        }

        if ($request->input('setupenv') === null) {
            throw new MaintenanceModeException(\Carbon\Carbon::now()->getTimestamp(), 30, "Canvas details API not configured");
        }

        if ($request->isMethod('POST')) {
            $values = $this->processData($request);
            if (!$values) {
                return AjaxForm::errorMessage(trans('caribe::setup.env_failed'))
                    ->jsonResponse(500);
            }

            // Write .env file
            $success = $this->writeNewEnvironmentFile($values);
            if (!$success) {
                return AjaxForm::errorMessage(trans('caribe::setup.env_failed'))
                    ->jsonResponse(500);
            }

            return AjaxForm::redirection(redirect()->back()->getTargetUrl())
                ->jsonResponse();
        }

        return response(view('setup.canvas_api'));
    }

    /**
     * Process the from data
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return mixed
     */
    protected function processData(Request $request)
    {
        return [
            'CANVAS_KEY' => $request->input('canvas_api_key'),
            'CANVAS_DOMAIN' => $request->input('canvas_api_domain')
        ];
    }

    /**
     * Create a .env file
     * 
     * @param array $data
     * 
     * @return boolean
     */
    protected function writeNewEnvironmentFile($data)
    {
        $content = '';
        foreach ($data as $key => $value) {
            if (is_bool($value)) {
                $value = $value ? 'true' : 'false';
            }
            $content .= "$key=$value\r\n";
        }

        return @file_put_contents(App::environmentFilePath(), $content, FILE_APPEND | LOCK_EX);
    }
}
