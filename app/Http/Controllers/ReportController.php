<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseStudent;
use App\Models\StudentReport;
use Caribe\Helpers\AjaxForm;
use Caribe\Helpers\MultiLanguage;
use Caribe\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Carbon;

class ReportController extends AdminController
{
    /**
     * Depending on the type of report:
     * Show the list of student access reports
     * Show the list of most visited pages reports
     * 
     * @param Request $request
     * @param string $reportType
     * @param Course|null $course
     * 
     * @return \Illuminate\Http\Response 
     */
    public function index(Request $request, string $reportType, Course $course = null)
    {
        $locale = $request->locale ?? MultiLanguage::defaultLanguage()->locale;
        $courseList = Course::where('locale', $locale)->oldest('title')->get(['id', 'title']);
        $courseSelected = (is_null($course)) ? $courseList[0] : $course;

        if ($reportType === 'access') {
            $reports = $this->createStudentsReports($courseSelected);
        } else if ($reportType === 'pages') {
            $reports = $this->createPagesReports($courseSelected);
        } else {
            abort(404);
        }

        return view(
            'admin.reports.index',
            compact('courseList', 'locale', 'reports', 'courseSelected', 'reportType')
        );
    }

    /**
     * Update course
     * 
     * @param  Request  $request
     * @param  Course   $course
     * 
     * @return \Illuminate\Http\Response
     */
    public function editStudentAccessForCourse(Request $request, Course $course)
    {
        if ($request->option !== 'open' && $request->option !== 'close') {
            abort(404);
        }

        $studentReport = StudentReport::where('id', $request->report)->first();
        $courseStudent = CourseStudent::where([
            'course_id' => $course->id,
            'student_id' => $studentReport->student->id
        ])->first();

        $courseStudent->time_course_finishes = ($request->option === "open") ? null : \Carbon\Carbon::now();
        $courseStudent->save();

        // Response
        return AjaxForm::custom(['message' => trans('admin.student_access_edit_success')])
            ->jsonResponse();
    }

    /**
     * Show all the access of a student to a scpecific course.
     * 
     * @param Course $course Course.
     * @param Student $student Student.
     * 
     * @return \Illuminate\Http\Response 
     */
    public function listStudentAccess(Course $course, Student $student)
    {
        $studentReports = StudentReport::where(['course_id' => $course->id, 'student_id' => $student->id])->get();

        foreach ($studentReports as $studentReport) {
            if (!is_null($studentReport->end_date_time)) {
                $studentReport['duration'] = Carbon::parse($studentReport->access_time)->diffInMinutes(Carbon::parse($studentReport->end_date_time));
            }
        }

        return view('admin.reports.student-access', compact('studentReports', 'course', 'student'));
    }

    /**
     * Depending on the type of report:
     * Download the access reports CSV file
     * Download the most visited pages reports CSV file
     *
     * @param string $reportType
     * @param Course $course
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadCsv(string $reportType, Course $course)
    {
        if ($reportType === 'access') {
            $reports = $this->createStudentsReports($course);
            $filename = "access-reports.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('name', 'nmail', 'first_access', 'last_access', 'current_page', 'visits_last_30_days'));

            foreach ($reports as $report) {
                fputcsv($handle, array($report['name'], $report['email'], $report['firstAccess'], $report['lastAccess'], $report['currentPage'], $report['numVisits']));
            }

            fclose($handle);
        } else if ($reportType === 'pages') {
            $reports = $this->createPagesReports($course);
            $filename = "popularity-reports.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('title', 'page_num_in_course', 'visits'));

            foreach ($reports as $report) {
                fputcsv($handle, array($report['title'], $report['position'], $report['numVisits']));
            }

            fclose($handle);
        } else {
            abort(404);
        }

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return response()->download($filename, $filename, $headers);
    }

    /**
     * Generate a list of student access reports.
     * 
     * @param Course $course
     * 
     * @return array The report. 
     */
    private function createStudentsReports(Course $course)
    {
        $report = [];

        foreach ($course->students as $courseStudent) {
            $student = $courseStudent->student;

            $studentReports = StudentReport::select('id', 'access_time', 'page_visited')
                ->where([
                    'course_id' => $course->id,
                    'student_id' => $student->id
                ])
                ->where('access_time', '>', \Carbon\Carbon::now()->subDays(30)->endOfDay())
                ->latest('access_time')->get();


            $report[] = [
                'id' => $studentReports[0]->id ?? null,
                'student_id' => $student->id,
                'name' => $student->name,
                'email' => $student->email,
                'firstAccess' => $courseStudent->first_access,
                'lastAccess' => $studentReports[0]->access_time ?? null,
                'timeCourseFinished' => $courseStudent->time_course_finishes,
                'currentPage' => $studentReports[0]->page_visited ?? null,
                'numVisits' => count($studentReports)
            ];
        }

        return $report;
    }

    /**
     * Generate the report of the most visited pages of a course .
     * 
     * @param Course $course
     * 
     * @return array The report. 
     */
    private function createPagesReports(Course $course)
    {
        $report = [];

        $mostVisitedPages = StudentReport::select('id', 'page_visited')
            ->selectRaw('COUNT(*) AS visits')
            ->where('course_id', $course->id)
            ->groupBy('page_visited')
            ->orderByDesc('visits')
            ->limit(10)
            ->get();

        foreach ($mostVisitedPages as $pageReport) {
            foreach ($course->items as $item) {
                if ($item->position === $pageReport->page_visited) {
                    $report[] = [
                        'id' => $pageReport->id,
                        'title' => $item->page->title,
                        'position' => $pageReport->page_visited,
                        'numVisits' => $pageReport->visits,
                    ];
                }
            }
        }

        return $report;
    }
}
