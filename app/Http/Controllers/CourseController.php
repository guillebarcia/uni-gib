<?php

namespace App\Http\Controllers;

use App\Http\Exceptions\InvalidDataException;
use App\Models\Course;
use App\Models\CourseItem;
use Caribe\Helpers\AjaxForm;
use Caribe\Helpers\MultiLanguage;
use Caribe\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends AdminController
{
    /**
     * Show the list of courses
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response 
     */
    public function index(Request $request)
    {
        $locale = $request->locale ?? MultiLanguage::defaultLanguage()->locale;
        $courses = Course::where('locale', 'en')->orderBy('title', 'asc')->get();

        return view('admin.courses.index', compact('courses', 'locale'));
    }

    /**
     * Show the create form
     * 
     * @return \Illuminate\Htpp\Response
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Create a new course
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Htpp\Response
     */
    public function store(Request $request)
    {
        try {
            $course = $this->processPost($request);
        } catch (InvalidDataException $e) {
            return AjaxForm::warningMessage($e->getMessage())
                ->jsonResponse(422);
        }

        return AjaxForm::custom([
            'message' => trans('admin.course_add_success'),
            'entryUrl' => route('admin.courses.edit', $course->id),
        ])->jsonResponse();
    }

    /**
     * Show the edit form
     * 
     * @param Course $course
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        $course->load(['items' => function ($query) {
            $query
                ->with('page')
                ->orderBy('position');
        }]);

        foreach ($course->items as $item) {
            if ($item->page == null) {
                $item->title = $item->title . ' (Not Found)';
            }
        }

        return view('admin.courses.edit', compact('course'));
    }

    /**
     * Update course
     * 
     * @param  Request  $request
     * @param  Course   $course
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        try {
            $this->processPost($request, $course);
        } catch (InvalidDataException $e) {
            return AjaxForm::warningMessage($e->getMessage())
                ->jsonResponse(422);
        }

        return AjaxForm::custom([
            'message' => trans('admin.course_edit_success'),
        ])->jsonResponse();
    }

    /**
     * Delete course
     * 
     * @param  Course   $course
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        // Delete
        $course->delete();

        // Response
        return AjaxForm::custom(trans('admin.course_delete_success'))
            ->jsonResponse();
    }

    /**
     * Process post (store and update)
     * 
     * @param  Request  $request
     * @param  Course|null  $course
     * 
     * @return Course
     */
    protected function processPost(Request $request, Course $course = null)
    {
        $request->merge([
            'items' => $this->parseItems($request),
        ]);

        // Validation
        $this->validateRequest($request, $course);

        DB::transaction(function () use (&$course, $request) {
            // Course
            if (!$course) {
                $course = new Course();
            }
            $course->fill($request->except('items'));
            $course->save();

            // Items
            $position = 1;
            $idMappings = [];
            $originalItems = $course->items->pluck('id')->toArray();
            foreach ($request->items as $item) {
                if ($item['parent_id'] && !isset($idMappings[$item['parent_id']])) {
                    throw new \Exception();
                }

                if (is_numeric($item['id'])) {
                    // Existing item
                    $courseItem = $this->extractItem($course->items, $originalItems, $item['id']);
                    if (!$courseItem) {
                        throw new \Exception();
                    }
                } else {
                    // New item
                    $courseItem = new CourseItem();
                }

                $courseItem->fill($item);
                $courseItem->course_id = $course->id;
                $courseItem->parent_id = $item['parent_id'] ? $idMappings[$item['parent_id']] : null;
                $courseItem->position = $position;

                $courseItem->save();

                $idMappings[$item['id']] = $courseItem->id;
                $position++;
            }

            // Delete remaining
            CourseItem::destroy($originalItems);
        });

        return $course;
    }

    /**
     * Parse and generate array from combined item data
     * 
     * This "special way" of combining the data before sending
     * has the purpose of reducing the total input fields sent,
     * which is limited in PHP (default 1000) and is easily
     * reached because of the number of fields for each item
     * 
     * @param  Request  $request
     * 
     * @return array
     */
    protected function parseItems(Request $request)
    {
        $result = [];
        if ($request->items) {
            $separator = '|@$@|';
            $fields = [
                'id', 'title', 'subtitle', 'page_id', 'link', 'target_blank',
                'css_id', 'css_class', 'parent_id', 'hidden'
            ];
            foreach ($request->items as $item) {
                $itemData = explode($separator, (string)$item);
                $item = [];
                foreach ($fields as $key => $field) {
                    $value = $itemData[$key] ?? null;
                    $item[$field] = $value === null || $value === "" ? null : $value;
                }
                $result[] = $item;
            }
        }
        return $result;
    }

    /**
     * Validate request
     * 
     * @param  Request  $request
     * @param  Course|null  $course
     * 
     * @return void
     */
    protected function validateRequest(Request $request, Course $course = null)
    {
        Validator::extend('uniqueCourse', function ($attribute, $value, $parameters, $validator) use ($course) {
            $query = DB::table('course')
                ->where('tag', $value)
                ->where('locale', $parameters[0]);
            if ($course) {
                $query->where('id', '<>', $course->id);
            }

            return $query->count() === 0;
        }, 'There\'s already a course with this tag');

        $request->merge([
            'published' => $request->has('published'),
        ]);

        // Validation
        $request->validate([
            'title' => 'required|max:150',
            'locale' => 'required|max:10',
            'tag' => [
                'bail',
                'required',
                'max:50',
                'alpha_dash',
                'uniqueCourse:' . $request->locale,
            ],
        ]);
    }

    /**
     * Extract an item from the list of items
     * 
     * @param  Collection  $items
     * @param  array  &$originalItems
     * @param  int  $id
     * 
     * @return CourseItem|null
     */
    protected function extractItem(Collection $items, array &$originalItems, int $id)
    {
        foreach ($items as $item) {
            if ($item->id == $id) {
                if (($key = array_search($id, $originalItems)) !== false) {
                    unset($originalItems[$key]);
                }

                return $item;
            }
        }

        return null;
    }
}
