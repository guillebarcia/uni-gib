<?php

namespace App\Http\Controllers;

use App\Helpers\Canvas\CoursesResources;
use App\Models\Course;
use App\Models\CourseItem;
use App\Models\CourseStudent;
use App\Models\Student;
use App\Models\StudentReport;
use Caribe\Helpers\MultiLanguage;
use Caribe\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FrontController extends Controller
{
    /**
     * Render a course page if the user is enrolled in it.
     * 
     * @param  Request $request
     * @param  string $courseTag
     * @param  string $itemPosition
     *
     * @return \Illuminate\Http\Response
     */
    public function render(Request $request, $courseTag, $itemPosition = 1)
    {
        // Gets the "Coruse" and the "ItemCourse" that has that position. 
        $course = Course::where([
            ['tag', $courseTag],
            ['locale', $request->locale ?? MultiLanguage::defaultLanguage()->locale],
        ])->with(['items' => function ($query) use ($itemPosition) {
            $query
                ->where('position', $itemPosition)
                ->with('page');
        }])->first();

        if (!$course || !count($course->items)) {
            abort(404);
        }

        if (env('CANVAS_TEST_ACCESS_MODE') !== true) {
            // Get the student id from the session, check if it exist and if it belongs to the course.
            $canvasUserId = session('studentId', null);
            $firstAccessFromCanvas = session('firstAccessFromCanvas', null);
            $student = Student::where('id', $canvasUserId)->first();

            if (!$student || !$this->isStudentInCourse($student->courses, $course)) {
                abort(403, 'Please enter via Canvas.');
            } else if ($this->studentFinishedCourse($student->courses, $course)) {
                abort(403, 'You finished the course. Please contact Canvas admin to start the course again.');
            }

            // Add the date and time the student leave the course page.
            if (is_null($firstAccessFromCanvas)) {
                $actualDate = \Carbon\Carbon::now();
                $this->addEndDateTimeToStudentReport($course, $student, $actualDate);
            } else {
                session()->forget('firstAccessFromCanvas');
            }

            // Create a new access report.
            $newStudent = StudentReport::create([
                'student_id' => $student->id,
                'course_id' => $course->id,
                'page_visited' => $itemPosition
            ]);
        }

        // Get the course page. 
        $page = $course->items[0]->page;

        // Get the total pages of the course.
        $totalPages = (count($course->items))
            ? CourseItem::where('course_id', $course->items[0]->course_id)->count()
            : 0;

        return view(
            'public.course',
            compact(
                'courseTag',
                'itemPosition',
                'page',
                'totalPages'
            )
        );
    }

    /**
     * The POST url "courses/{course}/access" is the url that is configured in canvas to redirect to a course.
     * If the student uses this path directly in the browser (GET "courses/{course}/access") a 404 error is displayed.
     * Adding this controller will display a 403 error and the student can go to Canvas and get the necessary permissions.
     * 
     * @return Abort 403. Not authorized
     */
    public function accessNotAuthorized()
    {
        abort(403, 'Please enter via Canvas.');
    }

    /**
     * Check if the user is enroll in the course.
     * If it is the first time that the user accesses the course, the first page is render.
     * If the user has accessed the course before, the last page he visited is rendered.
     * 
     * @param  Request $request
     * @param  string $courseTag
     * 
     * @return \Illuminate\Http\Response
     */
    public function access(Request $request, $courseTag)
    {
        // Get Canvas parameters.
        $canvasCourseId = $request->post('custom_canvas_course_id');
        $canvasUserId = $request->post('custom_canvas_user_id');
        $canvasUserEmail = $request->post('lis_person_contact_email_primary');

        // Check with the Canvas API that the user is registered in the course
        $canvasStudent = CoursesResources::getUserCourse($canvasCourseId, $canvasUserId);
        $course = Course::where('tag', $courseTag)->first();

        if (isset($canvasStudent->id) && $course) {
            $student = Student::where(['canvas_id' => $canvasUserId])->first();

            if (!is_null($student)) {
                // Check if id a student of the course 
                if (!$this->isStudentInCourse($student->courses, $course)) {
                    CourseStudent::create([
                        'course_id' => $course->id,
                        'student_id' => $student->id
                    ]);
                    $lastPageVisited = '1';
                } else {
                    $courseStudent = CourseStudent::select('time_course_finishes')
                        ->where([
                            'course_id' => $course->id,
                            'student_id' => $student->id
                        ])->first();

                    // Check if the user finished the course
                    if (is_null($courseStudent->time_course_finishes)) {
                        // Get the last page visited.
                        $lastStudentReport = StudentReport::where([
                            'student_id' => $student->id,
                            'course_id' => $course->id
                        ])->latest('access_time')->first();
                        $lastPageVisited = (is_null($lastStudentReport)) ? '1' : $lastStudentReport->page_visited;
                    } else {
                        abort(403, 'You finished the course. Please contact Canvas admin to start the course again.');
                    }
                }

                session(['studentId' => $student->id]);
                session(['firstAccessFromCanvas' => true]);

                // Redirect to the course and the last page visited.
                return redirect('/courses/' . $course->tag . '/' . $lastPageVisited);
            } else {
                // If the student doesn't exist, it will be created.
                $newStudent = Student::create([
                    'canvas_id' => $canvasStudent->id,
                    'name' => $canvasStudent->name ?? '',
                    'email' => $canvasUserEmail ?? '',
                ]);

                // Add the student to the course.
                CourseStudent::create([
                    'course_id' => $course->id,
                    'student_id' => $newStudent->id
                ]);

                session(['studentId' => $newStudent->id]);
                session(['firstAccessFromCanvas' => true]);

                // Redirect to the course.
                return redirect('/courses/' . $course->tag . '/1');
            }
        } else {
            abort(403, 'Please enter via Canvas.');
        }
    }

    /**
     * TODO: Add the time when the user finish the course by himself.
     * TODO: When the course is closed by the admin it shoudn't update the StudentReports table.
     *
     * Add the date the student completes the course.
     * Redirect to the course assignments url.
     * 
     * @param  string $courseTag
     * 
     * @return \Illuminate\Http\Response
     */
    public function finishCourse(string $courseTag)
    {
        $course = Course::where('tag', $courseTag)->first();

        // Get the student id from the session
        $canvasUserId = session('studentId', null);
        $student = Student::where('id', $canvasUserId)->first();

        // Check if the course exists, check if the student exist and if it belongs to the course.
        if (!$course || !$student || !$this->isStudentInCourse($student->courses, $course)) {
            abort(403, 'Please enter via Canvas.');
        }

        $actualDate = \Carbon\Carbon::now();

        $this->addEndDateTimeToStudentReport($course, $student, $actualDate);

        $courseStudent = CourseStudent::where([
            'course_id' => $course->id,
            'student_id' => $student->id
        ])->first();
        $courseStudent->time_course_finishes = $actualDate;
        $courseStudent->save();

        return response();
    }

    /**
     * Check if the student belongs to the course.
     * 
     * @param array Student course list.
     * @param Course Course he is trying to access
     * 
     * @return boolean
     */
    private function isStudentInCourse(Collection $studentCourses, Course $newCourse)
    {
        foreach ($studentCourses as $course) {
            if ($course['course_id'] == $newCourse->id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the student finished the course.
     * 
     * @param array Student course list.
     * @param Course Course he is trying to access
     * 
     * @return boolean
     */
    private function studentFinishedCourse(Collection $studentCourses, Course $course)
    {
        foreach ($studentCourses as $studentCourse) {
            if ($studentCourse['course_id'] == $course->id) {
                return (!is_null($studentCourse['time_course_finishes'])) ? true : false;
            }
        }

        return false;
    }

    /**
     * Update the field "end_date_time" with the actual date and time.
     * 
     * @param Course $course Course.
     * @param Student $student Student.
     */
    private function addEndDateTimeToStudentReport(Course $course, Student $student, \Carbon\Carbon $actualDate)
    {
        $studentReport = StudentReport::where([
            'student_id' => $student->id,
            'course_id' => $course->id,
        ])->latest('access_time')->first();

        if ($studentReport) {
            $studentReport->end_date_time = $actualDate;
            $studentReport->save();
        }
    }
}
