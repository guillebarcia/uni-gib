<?php

namespace App\Providers;

use Caribe\Http\Providers\AdminMenuServiceProvider;
use Caribe\Menu\Items\MenuGroup;
use Caribe\Menu\Items\MenuLink;
use Caribe\Menu\MenuManager;
use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->get('menu.builder')->addGenerator('admin', function (MenuManager $menuManager) {

            $menuManager->addItem(
                AdminMenuServiceProvider::SIDEBAR_MENU,
                (new MenuGroup(
                    'reports',
                    'Reports',
                    [
                        (new MenuLink('Access report', url('internal/reports/access')))
                            ->withIcon('fa fa-list')
                            ->withClasses(['action']),
                        (new MenuLink('Page popularity', url('internal/reports/pages')))
                            ->withIcon('fa fa-list')
                            ->withClasses(['active'])
                    ],
                    2500
                ))
                    ->withIcon('fa fa-users')
            );
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
