<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->setInjections();

        $this->app->get('acl')->addRole(
            \Caribe\Models\AdminUserAccessLevel::FULL_ADMINISTRATOR,
            new \App\Acl\Roles\CourseRole(),
            'admin'
        );

        $this->app->get('acl')->addRole(
            \Caribe\Models\AdminUserAccessLevel::FULL_ADMINISTRATOR,
            new \App\Acl\Roles\ReportRole(),
            'admin'
        );
    }

    /**
     * Set up injections
     *
     * @return void
     */
    protected function setInjections()
    {
        $this->app->get('injections')->add(
            'caribe::layouts.admin',
            'css',
            ['assets/admin/css/custom.css']
        );
    }
}
