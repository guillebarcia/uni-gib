<?php

namespace App\Providers;

use Caribe\Helpers\Menu;
use Caribe\Http\Providers\AdminMenuServiceProvider;
use Caribe\Menu\Items\MenuGroup;
use Caribe\Menu\Items\MenuLink;
use Caribe\Menu\MenuManager;
use Illuminate\Support\ServiceProvider;

class CourseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->get('menu.builder')->addGenerator('admin', function (MenuManager $menuManager) {

            $menuManager->addItem(
                AdminMenuServiceProvider::SIDEBAR_MENU,
                (new MenuGroup(
                    'courses',
                    'Courses',
                    [
                        (new MenuLink('Add new', url('internal/courses/create')))
                            ->withIcon('fa fa-plus')
                            ->withClasses(['action']),
                        (new MenuLink('View all', url('internal/courses')))
                            ->withIcon('fa fa-list')
                            ->withClasses(['active'])
                    ],
                    2500
                ))
                    ->withIcon('fa fa-graduation-cap')
            );
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
