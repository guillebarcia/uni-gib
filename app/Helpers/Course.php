<?php

namespace App\Helpers;

use Illuminate\Support\Facades\App;
use App\Models\Course as CourseModel;
use App\Models\CourseItem as CourseItemModel;
use Caribe\Menus\Contracts\Renderer as RendererContract;
use App\Renderers\CourseRenderer;

class Course
{
    /**
     * Get url for a courseItem
     * 
     * @param  App\Models\CourseItem  $courseItem
     * 
     * @return string
     */
    public static function itemUrl(CourseItemModel $courseItem)
    {
        return url("/courses/{$courseItem->course->tag}/{$courseItem->position}");
    }

    /**
     * Generate the course menu (HTML)
     *
     * @param  string  $tag
     * @param  string|null  $locale
     * @param  string|null  $renderer
     * 
     * @return string
     */
    public static function generateMenu(string $tag, string $locale = null, string $renderer = CourseRenderer::class)
    {
        $course = CourseModel::
            where([
                ['tag', $tag],
                ['locale', $locale ?: language()->locale],
            ])
            ->with(['items' => function ($query) {
                $query
                    ->with('page')
                    ->orderBy('position');
            }])
            ->first();
        if (!$course) {
            return '';
        }
        
        return self::renderMenu($course, $renderer);
    }
    
    /**
     * Generate the html
     *
     * @param  App\Models\Course  $course
     * @param  string  $renderer
     * 
     * @return string
     */
    protected static function renderMenu(CourseModel $course, string $renderer)
    {
        $courseItems = self::arrangeCourseItems($course);

        $renderer = new $renderer(App::get('app'));
        if (!($renderer instanceof RendererContract)) {
            throw new InvalidConfigurationException('Renderer does not implement Caribe\Menus\Contracts\Renderer interface');
        }
        return $renderer->render($courseItems, $course->css_id, $course->css_class);
    }

    /**
     * Arrange course items (nest properly)
     *
     * @param  App\Models\Course  $course
     * 
     * @return array
     */
    protected static function arrangeCourseItems(CourseModel $course)
    {
        $courseItems = [];
        
        foreach ($course->items as $item) {
            if (!$item->parent_id) {
                self::addItem($course, $courseItems, $item);
            }
            else {
                self::findParent($course, $courseItems, $item);
            }
        }
        
        return $courseItems;
    }

    /**
     * Find parent
     *
     * @param  App\Models\Course  $course
     * @param  array  &$list
     * @param  App\Models\CourseItem  $item
     * 
     * @return bool
     */
    protected static function findParent(CourseModel $course, array &$list, CourseItemModel $item)
    {
        if (isset($list[$item->parent_id])) {
            self::addItem($course, $list[$item->parent_id]['children'], $item);
            return true;
        }
        
        foreach ($list as &$listItem) {
            if ($listItem['children']) {
                $found = self::findParent($course, $listItem['children'], $item);
                if ($found) {
                    return true;
                }
            }
        }
        
        return false;
    }

    /**
     * Add item if available
     *
     * @param  App\Models\Course  $course
     * @param  array  &$parentList
     * @param  App\Models\CourseItem  $item
     * 
     * @return void
     */
    protected static function addItem(CourseModel $course, array &$parentList, CourseItemModel $item)
    {
        if ($course->hide_unavailable && $item->page_id && $item->page && !$item->page->isPublished()) {
            return;
        }
        if ($item->hidden) {
            return;
        }

        $parentList[$item->id] = [
            'item' => $item,
            'children' => [],
        ];
    }
}
