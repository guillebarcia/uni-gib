<?php

namespace App\Helpers;

class Curl
{

	private $token;
	private $baseUrl;
	private $curl;

	public function __construct(string $token, string $domain)
	{
		$this->token = $token;
		$this->baseUrl = 'https://' . $domain . '/api/v1';
		$this->initCurl();
	}

	/**
	 * Request a CURL petition with GET method.
	 * 
	 * @param string url.
	 * 
	 * @return object CURL response
	 */
	public function get(string $url)
	{
		curl_setopt($this->curl, CURLOPT_URL, $this->baseUrl . $url);
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'GET');
		$response = curl_exec($this->curl);
		$this->closeCurl();

		return json_decode($response);
	}

	/**
	 * Close CURL conection.
	 */
	public function closeCurl()
	{
		curl_close($this->curl);
	}

	/**
	 * Initialize a CURL call
	 */
	private function initCurl()
	{
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $this->token));
	}
}
