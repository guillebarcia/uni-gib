<?php

namespace App\Helpers\Canvas;

use App\Helpers\Curl;

class CoursesResources
{

	/**
	 * Get information of a single user enrroll in a course.
	 * 
	 * @param string $courseId
	 * @param string $userId
	 * 
	 * @return object Api response.
	 */
	public static function getUserCourse(string $courseId, string $userId)
	{
		$curl = new Curl(env('CANVAS_KEY'), env('CANVAS_DOMAIN'));
		$response = $curl->get('/courses/' . $courseId . '/users/' . $userId);
		return $response;
	}
}
