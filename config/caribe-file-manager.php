<?php

return [
    
    'uploads_folder' => 'uploads',
    
    'valid_image_mimetypes' => [
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/tiff',
        'image/gif',
        'image/bmp',
        'image/svg+xml',
        'image/x-icon',
    ],
    'valid_file_mimetypes' => [
        // Images
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/gif',
        'image/bmp',
        'image/svg+xml',
        'image/x-icon',
        'image/tiff',
        // Videos
        'video/mp4',
        'video/video/mpeg',
        'video/webm',
        // Files
        'text/plain',
        'text/csv',
        'application/rtf',
        'application/pdf',
        'application/msword',
        'application/vnd.ms-excel',
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        'application/vnd.oasis.opendocument.text',
        'application/vnd.oasis.opendocument.presentation',
        'application/vnd.oasis.opendocument.spreadsheet',
    ],
    
];
