<?php

use App\Helpers\Course as CourseHelper;
use App\Models\CourseItem;

if (!function_exists('course_menu')) {
    
    /**
     * Generate a menu from a course
     *
     * @param  string  $tag
     * @param  string|null  $locale
     * 
     * @return string
     */
    function course_menu(string $tag, string $locale = null)
    {
        return CourseHelper::generateMenu($tag, $locale);
    }
}

if (!function_exists('course_item_url')) {
    
    /**
     * Generate a menu from a course
     *
     * @param  CourseItem  $item
     * 
     * @return string
     */
    function course_item_url(CourseItem $item)
    {
        return CourseHelper::itemUrl($item);
    }
}
