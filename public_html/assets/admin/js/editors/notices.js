(function ($) {
    
    function Notices(visualEditor, $parent, view, initialData) {
        var noticesEditor = this;
        var isNoticeItem = false;
        var fieldPrefix = 'caribe-visual-editor-field-';
        var data = {};
        
        // Private functions
        function create() {
            var $this = $(view);
            $this.data('caribe-visual-editor', noticesEditor);
            
            return $this;
        }
        function loadModal() {
            var $modalContainer = $html.children('.caribe-visual-editor-modal');
            var modalHtml = $modalContainer[0].outerHTML;
            $modalContainer.remove();
            return modalHtml;
        }
        function generateModal() {
            var $modal = $(modalHtml);
            
            $modal.find('.caribe-visual-editor-field-type').on('change', function() {
                var number = $(this).children(':selected').data('number');
                $modal.find('.caribe-visual-editor-field-number').val(number);
            });
            $modal.find('.caribe-visual-editor-modal-button-discard').on('click', function() {
                closeModal($modal);
            });
            $modal.find('.caribe-visual-editor-modal-button-apply').on('click', function() {
                // Get form data
                $.each(fieldMapping(), function(field, functions) {
                    var $field = $modal.find('.' + field + ':first');
                    data[field.replace(fieldPrefix, '')] = $field.length > 0 ? functions.getter($field) : functions.defaultValue;
                });
                
                if (!isNoticeItem || data.type) {
                    updateHtml();
                    $htmlPlaceholder.show();
                }
                
                closeModal($modal);
            });
            
            return $modal;
        }
        function closeModal($modal) {
            CommonFunctions.closePopup();
        }
        function setUpEvents() {
            $html.find('.caribe-visual-editor-component-control-edit').on('click', function() {
                openEditPopup(false);
            });
            $html.find('.caribe-visual-editor-component-control-duplicate').on('click', function() {
                visualEditor.addElement('html', $html.parent(), noticesEditor.getData());
            });
            $html.find('.caribe-visual-editor-component-control-delete').on('click', function() {
                $html.remove();
            });
        }
        function initialize() {
            if (initialData) {
                var availableFields = Object.keys(fieldMapping());
                $.each(initialData, function(key, value) {
                    if (availableFields.indexOf(fieldPrefix + key) >= 0) {
                        data[key] = value;
                    }
                });
                if (data.category) {
                    data.category = data.category.split(',');
                }
                
                updateHtml();
                $htmlPlaceholder.show();
            }
        }
        function updateHtml() {
            $htmlPlaceholderContent.html(data.type == 'latest-notices' ? 'Latest Notices' : 'Notices Listing');
        }
        function openEditPopup(noticeItem) {
            isNoticeItem = noticeItem;
            
            var $modal = generateModal();
            
            // Load form data
            $.each(fieldMapping(), function(field, functions) {
                functions.setter($modal.find('.' + field + ':first'), data[field.replace(fieldPrefix, '')]);
            });
            if (!data.number) {
                $modal.find('.caribe-visual-editor-field-type').trigger('change');
            }
            
            CommonFunctions.openInlinePopup($modal, {
                modal: true,
                beforeShow: function() {
                    $modal.find('.caribe-visual-editor-field-category').selectpicker();
                    $modal.find('div.caribe-visual-editor-field-category').removeClass('caribe-visual-editor-field-category');
                },
                afterClose: function() {
                    if (isNoticeItem && !data.type) {
                        noticesEditor.destroy();
                    }
                }
            });
        }
        function fieldMapping() {
            return {
                // General
                'caribe-visual-editor-field-type': visualEditor.fieldMapping('dropdown'),
                'caribe-visual-editor-field-category': visualEditor.fieldMapping('dropdown'),
                'caribe-visual-editor-field-number': visualEditor.fieldMapping('input'),
                // Advanced
                'caribe-visual-editor-field-id': visualEditor.fieldMapping('input'),
                'caribe-visual-editor-field-class': visualEditor.fieldMapping('input'),
            };
        }
        
        // Public functions
        this.tag = 'notices';
        this.getData = function() {
            return jQuery.extend({}, data);
        }
        this.generate = function() {
            var props = [];
            $.each(data, function(key, value) {
                if (value) {
                    props.push(key + '="' + visualEditor.escapeQuotes(value) + '"');
                }
            });
            
            return '[notices ' + props.join(' ') + ' /]';
        }
        this.destroy = function() {
            $html.remove();
        }
        
        // Construct
        var $html = create();
        var $htmlPlaceholder = $html.children('.caribe-visual-editor-component:first');
        var $htmlPlaceholderContent = $htmlPlaceholder.find('.caribe-visual-editor-component-placeholder-content:first');
        var modalHtml = loadModal();
        setUpEvents();
        initialize();
        $parent.append($html);
        
        if (!initialData) {
            openEditPopup(true);
        }
    };
    
    Notices.title = 'Notices';
    Notices.icon = 'fa fa-file-text-o';
    
    $.caribeVisualEditor.editors['notices'] = Notices;
    
})(jQuery);
