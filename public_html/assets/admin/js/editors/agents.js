(function ($) {
    
    function Agents(visualEditor, $parent, view, initialData) {
        var externalNewsEditor = this;
        var isNewItem = false;
        var fieldPrefix = 'caribe-visual-editor-field-';
        var data = {};
        
        // Private functions
        function create() {
            var $this = $(view);
            $this.data('caribe-visual-editor', externalNewsEditor);
            
            return $this;
        }
        function loadModal() {
            var $modalContainer = $html.children('.caribe-visual-editor-modal');
            var modalHtml = $modalContainer[0].outerHTML;
            $modalContainer.remove();
            return modalHtml;
        }
        function generateModal() {
            var $modal = $(modalHtml);
            
            $modal.find('.caribe-visual-editor-modal-button-discard').on('click', function() {
                closeModal($modal);
            });
            $modal.find('.caribe-visual-editor-modal-button-apply').on('click', function() {
                // Get form data
                $.each(fieldMapping(), function(field, functions) {
                    var $field = $modal.find('.' + field + ':first');
                    data[field.replace(fieldPrefix, '')] = $field.length > 0 ? functions.getter($field) : functions.defaultValue;
                });
                   
                updateHtml();
                $htmlPlaceholder.show();
                
                closeModal($modal);
            });
            
            return $modal;
        }
        function closeModal($modal) {
            CommonFunctions.closePopup();
        }
        function setUpAgents() {
            $html.find('.caribe-visual-editor-component-control-edit').on('click', function() {
                openEditPopup(false);
            });
            $html.find('.caribe-visual-editor-component-control-duplicate').on('click', function() {
                visualEditor.addElement('html', $html.parent(), externalNewsEditor.getData());
            });
            $html.find('.caribe-visual-editor-component-control-delete').on('click', function() {
                $html.remove();
            });
        }
        function initialize() {
            if (initialData) {
                var availableFields = Object.keys(fieldMapping());
                $.each(initialData, function(key, value) {
                    if (availableFields.indexOf(fieldPrefix + key) >= 0) {
                        data[key] = value;
                    }
                });
                
                updateHtml();
                $htmlPlaceholder.show();
            }
        }
        function updateHtml() {
            $htmlPlaceholderContent.html('Agents');
        }
        function openEditPopup(newItem) {
            isNewItem = newItem;
            
            var $modal = generateModal();
            
            // Load form data
            $.each(fieldMapping(), function(field, functions) {
                functions.setter($modal.find('.' + field + ':first'), data[field.replace(fieldPrefix, '')]);
            });
            
            CommonFunctions.openInlinePopup($modal, {
                modal: true,
                afterClose: function() {
                    // Nothing
                }
            });
        }
        function fieldMapping() {
            return {
                // Advanced
                'caribe-visual-editor-field-id': visualEditor.fieldMapping('input'),
                'caribe-visual-editor-field-class': visualEditor.fieldMapping('input'),
            };
        }
        
        // Public functions
        this.tag = 'agents';
        this.getData = function() {
            return jQuery.extend({}, data);
        }
        this.generate = function() {
            var props = [];
            $.each(data, function(key, value) {
                if (value) {
                    props.push(key + '="' + visualEditor.escapeQuotes(value) + '"');
                }
            });
            
            return '[agents /]';
        }
        this.destroy = function() {
            $html.remove();
        }
        
        // Construct
        var $html = create();
        var $htmlPlaceholder = $html.children('.caribe-visual-editor-component:first');
        var $htmlPlaceholderContent = $htmlPlaceholder.find('.caribe-visual-editor-component-placeholder-content:first');
        var modalHtml = loadModal();
        setUpAgents();
        initialize();
        $parent.append($html);
        
        if (!initialData) {
            openEditPopup(true);
        }
    };
    
    Agents.title = 'Agents';
    Agents.icon = 'fa fa-address-card-o';
    
    $.caribeVisualEditor.editors['agents'] = Agents;
    
})(jQuery);
