$(document).ready(function () {
    ///////////////
    // Actions
    ///////////////

    $('.deleteEntry').click(function(e) {
        CommonFunctions.notificationConfirmDelete(
            "You are about to delete this feedback. This action cannot be undone",
            'Delete feedback',
            $(e.target).data('delete-url'),
            function(data) {
                CommonFunctions.notificationSuccessRedirect(data, listingUrl);
            }
        );
    });
});