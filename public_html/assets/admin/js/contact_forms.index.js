$(document).ready(function () {
    ///////////////
    // Actions
    ///////////////

    $('.deleteEntry').click(function(e) {
        CommonFunctions.notificationConfirmDelete(
            "You are about to delete this contact. This action cannot be undone",
            'Delete contact',
            $(e.target).data('delete-url'),
            function(data) {
                CommonFunctions.notificationSuccessRedirect(data, listingUrl);
            }
        );
    });
});