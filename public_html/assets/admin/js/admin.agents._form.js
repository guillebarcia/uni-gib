function formCallback(data){
    CommonFunctions.notificationSuccessStayOrBack(data.message, data.entryUrl, listingUrl)
}

$(document).ready(function() {
    ///////////////
    // Actions
    ///////////////
    
    $('#deleteEntry').click(function() {
        CommonFunctions.notificationConfirmDelete(
            "You are about to delete this agent. This action cannot be undone",
            'Delete agent',
            deleteUrl,
            function(data) {
                CommonFunctions.notificationSuccessRedirect(data, listingUrl);
            }
        );
    });
});
