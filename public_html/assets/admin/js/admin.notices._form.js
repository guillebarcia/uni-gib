function formCallback(data){
    CommonFunctions.notificationSuccessStayOrBack(data.message, data.entryUrl, listingUrl)
}

$(document).ready(function() {

    $('#f-date-datetimepicker').datetimepicker({
        format: 'L'
    });

    ///////////////
    // Actions
    ///////////////
    
    $('#deleteEntry').click(function() {
        CommonFunctions.notificationConfirmDelete(
            "You are about to delete this notice. This action cannot be undone",
            'Delete notice',
            deleteUrl,
            function(data) {
                CommonFunctions.notificationSuccessRedirect(data, listingUrl);
            }
        );
    });
});
