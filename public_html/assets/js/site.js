$(document).ready(function() {

	//Clean Empty paragraphs
    $('p').filter(function () { return $.trim(this.innerHTML) == "" }).remove();
    $('p').filter(function () { return this.innerHTML == "" }).remove();

    //Animate anchors
    $(".anchor").click(function(e) {
        e.preventDefault();
        var aid = $(this).attr("href");
        $('html,body').animate({scrollTop: ($(aid).offset().top)-60},'slow');
    });

    AOS.init();

    //Update Horizontal Right Align
    $(".justify-content-right").addClass("justify-content-end").removeClass("justify-content-right");

    //data-href links
    $('[data-href*="/"]').click(function(){
        location.href = $(this).attr("data-href");
    });

    // Generate email links
    $("a[data-email-to-name][data-email-to-domain]").each(function() {
        var $this = $(this);
        if (!$this.attr("data-email-to-completed")) {
            var address = $this.attr("data-email-to-name") + String.fromCharCode(64) + $this.attr("data-email-to-domain");
            $this.attr("href", "mailto:" + address);
            $this.html('' + address);
            $this.attr("data-email-to-completed", true);
        }
    });

    //Responsice Image Maps
    $('img[usemap]').rwdImageMaps();

    // Accesibility
    $("#accessibility-btn").on("click", function(e) {
        e.preventDefault();
        showAccessibilityPanel();
    });

    //Menu
    $('#open-menu, #close-menu').on('click', function(){
        $('#offCanvas').toggleClass('active');
        return false;
    });

    //Modals
    $('.open-modal').on('click', function(e){
        e.preventDefault();
        $($(this).attr('href')).addClass('active');
        jQuery.fn.matchHeight._update();
    });

    $('map > area').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        if ($('.small-modal.active').length == 0) {
            $($(this).attr('href')).addClass('active');
        }
    });

    $(window).on('click', function(e){
        $('.small-modal').removeClass('active');
    });

    $('.close-uni-modal').on('click', function(e){
        e.preventDefault();
        $(this).parents('.uni-modal').removeClass('active');
    });

    $('.close-small-modal').on('click', function(e){
        e.preventDefault();
        $(this).parents('.small-modal').removeClass('active');
    });

    //Questions
    $('#question:not(.multiple) .answer').on('click', function(e){
        $('.answer').removeClass('active');
        $(this).toggleClass('active');
    });

    $('#question.multiple .answer').on('click', function(e){
        $(this).toggleClass('active');
    });
});
