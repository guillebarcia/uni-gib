var $accesibilityPanel = null;
function showAccessibilityPanel() {
    if ($accesibilityPanel) {
        $accesibilityPanel.animate({right: 0});
    }
}

$(document).ready(function() {

    $accesibilityPanel = $('#accesibilityPanel');
    var $accesibilityArea = $('#accesibilityArea');
    var $fontElements = null;
    var fontSize = 0;
    var colorFilter = null;
    var highlight = null;
    var colour = null;
    var zoom = 1;

    $('#closeAccesibilityPanel').click(function() {
        var width = parseInt($accesibilityPanel.css('width').slice(0, -2));
        $accesibilityPanel.animate({right: -(width + 25)});
    });

    function savePreferences() {
        sessionStorage.setItem('acc-prefs', JSON.stringify({
            font: fontSize,
            filter: colorFilter,
            highlight: highlight,
            colour: colour,
            zoom: zoom,
        }));
    }
    function loadPreferences() {
        var prefs = sessionStorage.getItem('acc-prefs');
        if (prefs) {
            try {
                prefs = JSON.parse(prefs);

                fontSize = prefs.font;
                colorFilter = prefs.filter;
                highlight = prefs.highlight;
                colour = prefs.colour;
                zoom = prefs.zoom;

                updateForm();
                
                updateFontSize();
                updateFilter();
                updateHightlight();
                updateColour();
                updateZoom();
            } catch (e) {
            }
        }
        else {
            updateLabels();
        }
    }
    function updateForm() {
        $('[name="accessibilityFiltersOptions"][value="' + colorFilter + '"]').prop("checked", true);
        $('[name="accessibilityHighlightOptions"][value="' + highlight + '"]').prop("checked", true);
        $('[name="accessibilityColourOptions"][value="' + colour + '"]').prop("checked", true);
    }
    function updateLabels() {
        $('.accesibilityFontSize').html(20 + fontSize);
        $('.accesibilityZoomScale').html(Math.round(zoom * 100) / 100);
    }

    // Font
    $('.accesibilityFontIncrease').click(function() {
        fontSize += 2;
        updateFontSize();
        savePreferences();
    });
    $('.accesibilityFontDecrease').click(function() {
        fontSize = Math.max(fontSize - 2, -18);
        updateFontSize();
        savePreferences();
    });
    $('.accesibilityFontReset').click(function() {
        fontSize = 0;
        updateFontSize();
        savePreferences();
    });
    function updateFontSize() {
        if ($fontElements == null) {
            $fontElements = $accesibilityArea.find('h1,h2,h3,h4,h5,h6,p,a,span,li');
        }
        $fontElements.each(function() {
            var $this = $(this);
            if (!$this.data('original-size')) {
                var originalSize = parseInt($this.css('font-size').slice(0, -2));
                $this.data('original-size', originalSize);
            }
            
            $this
                .css('font-size', $this.data('original-size') + fontSize + 'px')
                .css('line-height', $this.data('original-size') + fontSize + 5 + 'px');
        });
        updateLabels();
    }

    // Filters
    $('[name="accessibilityFiltersOptions"]').click(function() {
        colorFilter = $(this).val();
        updateFilter();
        savePreferences();
    });
    function updateFilter() {
        $accesibilityArea.removeClass('acc-filter-grayscale acc-filter-invert acc-filter-w-blue acc-filter-w-green');
        if (colorFilter) {
            $accesibilityArea.addClass('acc-filter-' + colorFilter);
        }
    }

    // Highlight
    $('[name="accessibilityHighlightOptions"]').click(function() {
        highlight = $(this).val();
        updateHightlight();
        savePreferences();
    });
    function updateHightlight() {
        $accesibilityArea.removeClass('acc-highlight-links acc-highlight-titles acc-highlight-text');
        if (highlight) {
            $accesibilityArea.addClass('acc-highlight-' + highlight);
        }
    }

    // Colours
    $('[name="accessibilityColourOptions"]').click(function() {
        colour = $(this).val();
        updateColour();
        savePreferences();
    });
    function updateColour() {
        $accesibilityArea.removeClass('acc-colour-black acc-colour-yellow acc-colour-green acc-colour-white');
        if (colour) {
            $accesibilityArea.addClass('acc-colour-' + colour);
        }
    }

    // Zoom
    $('.accesibilityZoomIncrease').click(function() {
        zoom += 0.1;
        updateZoom();
        savePreferences();
    });
    $('.accesibilityZoomDecrease').click(function() {
        zoom = Math.max(zoom - 0.1, 0.1);
        updateZoom();
        savePreferences();
    });
    $('.accesibilityZoomReset').click(function() {
        zoom = 1;
        updateZoom();
        savePreferences();
    });
    function updateZoom() {
        $accesibilityArea
            .css('transform', 'scale(' + zoom + ')')
            .css('transform-origin', '0 0 0');
        updateLabels();
    }

    loadPreferences();

});
