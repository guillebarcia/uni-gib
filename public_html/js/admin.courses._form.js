var $menu = null;
var itemHtml = null;
var newItemPrefix = "NI";
var newItemCounter = 0;

function addItem($li, parentId, id, animate) {
    var $hideIcon = $li.find(".hideMenuItem i");
    $hideIcon.addClass(
        $li.data("hidden")
            ? $hideIcon.data("off-class")
            : $hideIcon.data("on-class")
    );
    if ($li.data("hidden")) {
        $li.addClass("greyed-out");
    }

    if (!id) {
        id = newItemPrefix + newItemCounter;
    }
    $li.attr("id", "item" + id);
    $li.data("id", id);

    var $container = $menu;
    if (parentId) {
        $container = $menu.find("#item" + parentId);
        $container.addClass("dd-collapsed");
    }

    var $ol = $container.children("ol:first");
    if ($ol.length == 0) {
        $ol = $('<ol class="dd-list"></ol>').appendTo($container);
    }

    if (animate) {
        $li.hide().appendTo($ol).show("slow");
    } else {
        $ol.append($li);
    }

    newItemCounter++;
}

function addPage(
    title,
    subtitle,
    pageId,
    pageTitle,
    pageUrl,
    targetBlank,
    cssId,
    cssClass,
    hidden,
    parentId,
    id,
    animate,
    found
) {
    var $newItem = $(itemHtml);

    (found) ? $newItem.find(".dd-handle").addClass("dd3-handle") : $newItem.find(".dd-handle").addClass("dd3-not-found");
    $newItem.find(".main-icon").addClass("fa fa-file-text-o");
    $newItem.find("span").html(title);

    $newItem.attr("data-page-title", pageTitle);
    $newItem.attr("data-page-url", pageUrl);

    $newItem.data("page_id", pageId);
    $newItem.data("title", title);
    $newItem.data("subtitle", subtitle);
    $newItem.data("link", "");
    $newItem.data("target_blank", targetBlank);
    $newItem.data("css_id", cssId);
    $newItem.data("css_class", cssClass);
    $newItem.data("hidden", hidden);

    addItem($newItem, parentId, id, animate);
}

function addPageCallback(page) {
    addPage(page.title, "", page.id, page.title, page.url, false, "", "", false);
}

$(document).ready(function () {
    $menu = $("#menuItems");
    itemHtml = $("#itemHtml").html();
    var $form = $menu.closest("form");

    ////////////////////////
    // Populate list
    ////////////////////////

    $.each(items, function (key, item) {
        if (item["page"] != null) {
            addPage(
                item["title"],
                item["subtitle"],
                item["page_id"],
                item["page"]["title"],
                item["page"]["complete_url"] ? item["page"]["complete_url"] : "",
                item["target_blank"] == "1",
                item["css_id"],
                item["css_class"],
                item["hidden"] == "1",
                item["parent_id"],
                item["id"],
                false,
                true
            );
        } else {
            addPage(
                item["title"],
                item["subtitle"],
                "",
                "",
                "",
                item["target_blank"] == "1",
                item["css_id"],
                item["css_class"],
                item["hidden"] == "1",
                item["parent_id"],
                item["id"],
                false,
                false
            );
        }
    });

    $(".dd").nestable();

    ////////////////////////
    // Item events
    ////////////////////////

    $(document).on("click", "#menuItems .editMenuItem", function (e) {
        var $this = $(this);
        var $li = $this.closest("li");

        var $window = null;
        $window = $($("#editPageModal").html());
        $window
            .find(".pageInfo")
            .html(
                "<strong>" +
                $li.attr("data-page-title") +
                "</strong> (" +
                $li.attr("data-page-url") +
                ")"
            );
        $window.find('input[data-name="title"]').val($li.data("title"));
        $window.find('input[data-name="subtitle"]').val($li.data("subtitle"));
        $window
            .find('input[data-name="target_blank"]')
            .prop("checked", $li.data("target_blank"));
        $window.find('input[data-name="css_id"]').val($li.data("css_id"));
        $window.find('input[data-name="css_class"]').val($li.data("css_class"));
        $window.find(".saveButton").on("click", function () {
            var title = $window.find('input[data-name="title"]').val();
            if (title) {
                $li.children("div").children("span").html(title);

                $li.data("title", title);
                $li.data(
                    "subtitle",
                    $window.find('input[data-name="subtitle"]').val()
                );
                $li.data(
                    "target_blank",
                    $window.find('input[data-name="target_blank"]').prop("checked")
                );
                $li.data("css_id", $window.find('input[data-name="css_id"]').val());
                $li.data(
                    "css_class",
                    $window.find('input[data-name="css_class"]').val()
                );

                CommonFunctions.closePopup();
            }
        });

        $window.find(".cancelButton").on("click", function () {
            CommonFunctions.closePopup();
        });
        CommonFunctions.openInlinePopup($window, { modal: true });
    });
    $(document).on("click", "#menuItems .hideMenuItem", function (e) {
        var $this = $(this);
        var $li = $this.closest("li");
        var hidden = $li.data("hidden");
        $li.data("hidden", !hidden);

        hidden ? $li.removeClass("greyed-out") : $li.addClass("greyed-out");
        var $hideIcon = $this.children("i");
        var newClass = $hideIcon.data("alt-class");
        $hideIcon
            .removeClass(
                hidden ? $hideIcon.data("off-class") : $hideIcon.data("on-class")
            )
            .addClass(
                hidden ? $hideIcon.data("on-class") : $hideIcon.data("off-class")
            );
        $hideIcon.attr("class", newClass);
    });
    $(document).on("click", "#menuItems .deleteMenuItem", function (e) {
        var $li = $(this).closest("li");
        $li.hide("slow", function () {
            $li.remove();
        });
    });

    ////////////////////////
    // Add buttons
    ////////////////////////

    $(".addPage").click(function () {
        var locale = $("#f-locale").val();
        CommonFunctions.openIframePopup(
            pageSelectionUrl +
            "?callback=addPageCallback&multiple=true&locale=" +
            locale
        );
    });

    ////////////////////////
    // Submit
    ////////////////////////

    $('button[type="submit"]').click(function (e) {
        function generateDataField(data) {
            var fields = [
                "id",
                "title",
                "subtitle",
                "page_id",
                "link",
                "target_blank",
                "css_id",
                "css_class",
                "parent_id",
                "hidden",
            ];
            var values = [];
            $.each(fields, function () {
                var value = data[this];
                if (typeof value == "boolean") {
                    value = value ? "1" : "0";
                }
                values.push(value);
            });
            return values.join("|@$@|");
        }
        function processLi($li, parentId) {
            var data = $li.data();
            data["parent_id"] = parentId;
            var fieldValue = generateDataField(data);
            $('<input type="hidden" class="autogenerated-field" />')
                .attr("name", "items[" + data.id + "]")
                .val(fieldValue)
                .appendTo($form);

            var $children = $li.children("ol").children("li");
            if ($children.length > 0) {
                $.each($children, function (key, li) {
                    processLi($(li), data.id);
                });
            }
        }

        $(".autogenerated-field").remove();
        $.each($menu.children("ol").children("li"), function (key, li) {
            processLi($(li), "");
        });
    });
});
